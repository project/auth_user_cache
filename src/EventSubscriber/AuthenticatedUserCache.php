<?php


namespace Drupal\auth_user_cache\EventSubscriber;


use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class AuthenticatedUserCache
 *
 * @package Drupal\auth_user_cache\EventSubscriber
 */
class AuthenticatedUserCache implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * AuthenticatedUserCache constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   * @param \Drupal\Core\State\StateInterface $state
   */
  public function __construct(AccountProxyInterface $accountProxy, StateInterface $state, RequestStack $requestStack, CurrentPathStack $currentPathStack, PathMatcherInterface $pathMatcher) {
    $this->currentUser = $accountProxy;
    $this->state = $state;
    $this->request = $requestStack->getCurrentRequest();
    $this->currentPath = $currentPathStack;
    $this->pathMatcher = $pathMatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond', -200];
    return $events;
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   */
  public function onRespond(FilterResponseEvent $event) {
    if (!$event->isMasterRequest() || $this->currentUser->isAnonymous()) {
      return;
    }

    $response = $event->getResponse();
    if ($response instanceof HtmlResponse) {
      // If there is any user specific data on the page skip caching.
      $metadata = $response->getCacheableMetadata();
      if (in_array('user', $metadata->getCacheContexts())) {
        return;
      }
      $paths = $this->state->get('auth_user_cache.path_list', "/admin*\n/batch*\n/node/add*\n/node/*/edit\n/node/*/delete\n/user/*/edit*\n/user/*/cancel*");
      $path = $this->currentPath->getPath($this->request);
      if (!$this->pathMatcher->matchPath($path, $paths)) {
        $roles = $this->currentUser->getRoles();
        $cache_lifetime = [];
        foreach ($roles as $role) {
          $user_cache_time = $this->state->get('auth_user_cache.' . $role);
          if (!is_null($user_cache_time)) {
            $cache_lifetime[] = $user_cache_time;
          }
        }
        sort($cache_lifetime);
        $cache_lifetime = reset($cache_lifetime);
        if ($cache_lifetime) {
          $response->headers->removeCacheControlDirective('must-revalidate');
          $response->headers->removeCacheControlDirective('no-cache');
          $response->setPublic();
          $response->setMaxAge($cache_lifetime);
        }
        $response->headers->remove('Vary');
        $response->headers->set('X-Drupal-Roles', implode(' ', $roles));
        $response->setVary('Accept-Encoding, X-Drupal-Roles');
      }
    }
  }

}
